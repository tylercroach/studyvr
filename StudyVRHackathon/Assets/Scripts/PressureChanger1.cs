﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class PressureChanger1 : MonoBehaviour
	{
	public GameObject controllerleft, controllerright, mainobject;
	Vector3 distancevector;
	public int maxheight, minheight, speed, hapticmin; 
	float distance, previousdistance;
	private SteamVR_Controller.Device controller1 { get { return SteamVR_Controller.Input((int)leftcontroller.index); } }
	private SteamVR_Controller.Device controller2 { get { return SteamVR_Controller.Input((int)rightcontroller.index); } }
	private SteamVR_TrackedObject leftcontroller, rightcontroller;
	// Use this for initialization
	void Start()
		{
		mainobject = gameObject;
		distance = getDistance();
		previousdistance = distance;
		leftcontroller = controllerleft.GetComponent<SteamVR_TrackedObject>();
		rightcontroller = controllerright.GetComponent<SteamVR_TrackedObject>();
		}

	// Update is called once per frame
	void Update()
		{
		distance = getDistance();
		if (controller1.GetPress(SteamVR_Controller.ButtonMask.Trigger) || controller2.GetPress(SteamVR_Controller.ButtonMask.Trigger))
			{
			if (mainobject.transform.position.y >= minheight && mainobject.transform.position.y <= maxheight)
				{
                    if(previousdistance != distance)
                    {
                    float distancechange = distance - previousdistance;
                    if (distancechange < 0f) distancechange = distancechange * -1;
                    float percentchange = (distancechange) / previousdistance;
                    if(percentchange > .01f)
                    {
                        previousdistance = distance;
                        singlePulse();
                    }
                    }
				//ChangePressure();
				}
			}
		}
	float getDistance()
		{
		distancevector = controllerleft.transform.position - controllerright.transform.position;
		return (float)Math.Sqrt(Math.Pow(distancevector.x, 2) + Math.Pow(distancevector.y, 2) + Math.Pow(distancevector.z, 2));
		}
	void ChangePressure()
		{
		if (previousdistance - distance < -0.005f)
			{
			mainobject.transform.Translate(0, distance / (speed * -1), 0);
			if (mainobject.transform.position.y < minheight)
				{
				mainobject.transform.position = new Vector3(mainobject.transform.position.x, minheight, mainobject.transform.position.z);
				}
			}
		if (previousdistance - distance > 0.005f)
			{
			mainobject.transform.Translate(0, distance / speed, 0);
			HapticPulse();
			if (mainobject.transform.position.y > maxheight)
				{
				mainobject.transform.position = new Vector3(mainobject.transform.position.x, maxheight, mainobject.transform.position.z);
				}
			}
		previousdistance = distance;
		}
	void HapticPulse()
		{
		Debug.Log("Haptic Pulse");
		float dist = (mainobject.transform.position.y + 5) / 10;
		Debug.Log(dist);
		//if (dist > 0.8)
		//	{
		//	StartCoroutine(LongVibration(1, 1));
		//	}
			StartCoroutine(LongVibration(0.5f * dist, 0.5f * dist));
			}
    void singlePulse()//does a single pulse
    {
        SteamVR_Controller.Input((int)controllerleft.GetComponent<SteamVR_TrackedObject>().index).TriggerHapticPulse(3999);
        SteamVR_Controller.Input((int)controllerright.GetComponent<SteamVR_TrackedObject>().index).TriggerHapticPulse(3999);
    }
	public IEnumerator LongVibration(float length, float strength) {
    for(float i = 0; i < length; i += Time.deltaTime) {
        SteamVR_Controller.Input((int)controllerleft.GetComponent<SteamVR_TrackedObject>().index).TriggerHapticPulse((ushort)Mathf.Lerp(0, 3999, strength));
		SteamVR_Controller.Input((int)controllerright.GetComponent<SteamVR_TrackedObject>().index).TriggerHapticPulse((ushort)Mathf.Lerp(0, 3999, strength));
			if (controller1.GetPressUp(SteamVR_Controller.ButtonMask.Trigger) || controller2.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
				{
				break;
				}
        yield return null;
    }
}
	}