﻿using UnityEngine;
using System.Collections;

public class MovieTrigger : MonoBehaviour
	{
	public GameObject intro, fish, balloon, balloonobject, camera;
	public AudioClip wind, bubbles;
	// Use this for initialization
	void Start()
		{

		}

	// Update is called once per frame
	void Update()
		{

		}
	void OnTriggerEnter(Collider other)
		{
		if (other.gameObject.tag == "Intro")
			{
			camera.transform.parent = null;
			camera.transform.position = new Vector3(0, 0, 0);
			GetComponent<AudioSource>().Stop();
			intro.gameObject.GetComponent<PlayMovie>().Restart();
			fish.gameObject.GetComponent<PlayMovie>().movTexture.Stop();
			balloon.gameObject.GetComponent<PlayMovie>().movTexture.Stop();
			}
		if (other.gameObject.tag == "Fish")
			{
			camera.transform.parent = null;
			fish.gameObject.GetComponent<PlayMovie>().Restart();
			GetComponent<AudioSource>().PlayOneShot(bubbles);
			intro.gameObject.GetComponent<PlayMovie>().movTexture.Stop();
			balloon.gameObject.GetComponent<PlayMovie>().movTexture.Stop();
			}
				if (other.gameObject.tag == "Balloon")
			{
			camera.transform.position = new Vector3(camera.transform.position.x, 9.8f, camera.transform.position.z);
			intro.GetComponent<AudioSource>().Stop();
			camera.transform.parent = balloonobject.transform;
			balloon.gameObject.GetComponent<PlayMovie>().Restart();
			GetComponent<AudioSource>().PlayOneShot(wind);
			intro.gameObject.GetComponent<PlayMovie>().movTexture.Stop();
			fish.gameObject.GetComponent<PlayMovie>().movTexture.Stop();
			}
		}
	}
