﻿using UnityEngine;
using System.Collections;

public class SetSortingLayer : MonoBehaviour {
	public string layer;
	// Use this for initialization
	void Start()
		{
		if (GetComponent<MeshRenderer>() != null) {
			GetComponent<MeshRenderer>().sortingLayerName = layer;
		}
		if (GetComponent<SkinnedMeshRenderer>() != null)
			{
			GetComponent<SkinnedMeshRenderer>().sortingLayerName = layer;
			}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
